# Reference VPC state file
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "terraform-global-state-bucket"
    key    = "${var.application}/${var.env}/vpc/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "database" {
  backend = "s3"

  config {
    bucket = "terraform-global-state-bucket"
    key    = "${var.application}/${var.env}/database/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_ssm_parameter" "db_host" {
  name  = "/${var.env}/${var.application}/database/host"
}
data "aws_ssm_parameter" "db_password" {
  name  = "/${var.env}/${var.application}/database/password"
}

data "template_file" "user-data-app-server" {
  template = "${file("${path.module}/userdata.tpl")}"
  vars {
    host = "${data.aws_ssm_parameter.db_host.value}"
    password = "${data.aws_ssm_parameter.db_password.value}"
  }
}

module "app-server" {
  source          = "git::ssh://git@bitbucket.org/hanfi/terraform_modules.git//aws_alb_asg?ref=master"
  env             = "${var.env}"
  application     = "${var.application}"
  service         = "${var.service}"
  release_version = "${var.release_version}"
  vpc_id          = "${data.terraform_remote_state.vpc.vpc_id}"
  alb_port        = "${var.alb_port}"
  cidr_block      = "${var.cidr_block}"
  instance_type   = "${var.instance_type}"
  user_data       = "${var.user_data}"
  key_name        = "${var.key_name}"
  dns_zone_id     = "${data.terraform_remote_state.vpc.route53_private_zone}"
  asg_min         = "${var.asg_min}"
  asg_max         = "${var.asg_max}"
  asg_desired     = "${var.asg_desired}"
  user_data       = "${data.template_file.user-data-app-server.rendered}"

}

module "authorize-connection-to-db" {
  source                        = "git::ssh://git@bitbucket.org/hanfi/terraform_modules.git//authorize_link?ref=master"
  port                          = "${data.terraform_remote_state.database.port}"
  source_security_group_id      = "${module.app-server.security_group}"
  destination_security_group_id = "${data.terraform_remote_state.database.security_group}"
}

