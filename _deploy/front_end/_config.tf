provider "aws" {
  region = "${var.aws_region}"
}

data "aws_caller_identity" "current" {}

terraform {
  backend "s3" {
    bucket         = "terraform-global-state-bucket"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "terraform_locks"
  }

  required_version = ">= 0.11.3"
}
