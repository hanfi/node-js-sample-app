variable "env" {}
variable "application" {}
variable "service" {}
variable "release_version" {}

variable "aws_region" {}

variable "alb_port" {}
variable "cidr_block" {}

variable "instance_type" {}
variable "user_data" {}
variable "key_name" {}

variable "asg_min" {}
variable "asg_max" {}
variable "asg_desired" {}
