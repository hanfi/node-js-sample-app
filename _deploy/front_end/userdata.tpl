#!/bin/bash
mkdir /usr/src/app/config/
touch /usr/src/app/config/config.json 

cat <<EOF > /usr/src/app/config/config.json 
{
    "host": "${host}",
    "database": "mydatabase",
    "user": "dbusername",
    "password": "${password}",
    "port": "5432"
}
EOF

cat /usr/src/app/config/config.json
docker restart `docker ps -a -q`
