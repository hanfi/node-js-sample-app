variable "env" {}
variable "application" {}
variable "service" {}
variable "aws_region" {}
variable "db_engine" {}
variable "db_engine_version" {}
variable "db_instance_class" {}
variable "db_multi_az" {}
variable "db_allocated_storage" {}
variable "db_name" {}
variable "db_username" {}
variable "db_parameter_group" {}

variable "change_me_to_regenerate_password" {
  default = "0"
}
