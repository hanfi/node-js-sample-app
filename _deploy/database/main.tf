# Reference VPC state file
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "terraform-global-state-bucket"
    key    = "${var.application}/${var.env}/vpc/terraform.tfstate"
    region = "eu-west-1"
  }
}

resource "random_string" "password" {
  length = 16
  special = false
  keepers = {
    postgres_database = "${var.change_me_to_regenerate_password}"
  }
}

module "postgres-database" {
  source               = "git::ssh://git@bitbucket.org/hanfi/terraform_modules.git//rds_database?ref=master"
  env                  = "${var.env}"
  application          = "${var.application}"
  service              = "${var.service}"
  vpc_id               = "${data.terraform_remote_state.vpc.vpc_id}"
  db_engine            = "${var.db_engine}"
  db_engine_version    = "${var.db_engine_version}"
  db_instance_class    = "${var.db_instance_class}"
  db_multi_az          = "${var.db_multi_az}"
  db_allocated_storage = "${var.db_allocated_storage}"
  db_name              = "${var.db_name}"
  db_username          = "${var.db_username}"
  db_password          = "${random_string.password.result}"
  db_parameter_group   = "${var.db_parameter_group}"
  private_host_zone    = "${data.terraform_remote_state.vpc.route53_private_zone}"
}
output "port" {
  value = "${module.postgres-database.port}"
}

output "security_group" {
  value = "${module.postgres-database.security_group}"
}
