'use strict';
const os = require('os');
const express = require('express');
var AWS = require('aws-sdk');
const { Client } = require('pg')


// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

var pjson = require('./package.json');


// App
const app = express();
app.get('/', (req, res) => {
  var meta = new AWS.MetadataService();
  meta.request("/latest/meta-data/instance-id", function (err, data) {
    res.send('<h1>Hello Advances</h1><p> i m running on version '+pjson.version+' inside a container '+os.hostname()+' and here is my Hostname : ' + data+"</p>");
  });
});

app.get('/status', (req, res) => {
  res.send('200 OK\n');
});

app.get('/db', (req,res) => {
  var config = require('./config/config.json');
  const client = new Client({
    host: config.host,
    database: 'mydatabase',
    user: 'dbusername',
    password: config.password,
    port: 5432,  })
  client.connect()
  client.query('SELECT NOW()', (err, response) => {
    res.send(response)
    client.end()
  })
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);